﻿Imports System.Data.SqlClient
Imports System.Reflection
Imports HT_Production_Recorder.dsPackingSolutionTableAdapters

Public Class frmMain
#Region "< Members >"
    Dim nRowCount As Integer
    Dim nMakeQty As Integer
    Dim dTampdate As Date
    Dim bChgTamper As Boolean
    Dim nShiftHours As Integer
    Dim dvHTCrew As New DataView
    Dim dvJoMast As New DataView
    Dim dvProdSearch As New DataView
    Dim dvSchedSearch As New DataView
    Dim dvHTProd As New DataView
    Dim nShift As Integer
    Dim _sFac As String
    Dim _mGroup As String
    Private _SMTPClient As String
    private _m2mCatalog as String
    private _psCatalog as String
#End Region
    Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        My.Settings.screenposition = Me.Location
        My.Settings.sFac = _sFac
        My.Settings.mGroup = _mGroup
        My.Settings.Save()
    End Sub
    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dim appSetting = new AppSettings()

        dim sqlHelper = new SqlHelper(appSetting.GetConnectionString("M2M"))
        If (Not sqlHelper.IsConnected)
            dim frm = New frmSqlConfig
            frm.Title = "M2M"
            frm.ShowDialog(me)
        End If

        Dim psSqlHelper = new SqlHelper(appSetting.GetConnectionString("PS"))
        If (Not psSqlHelper.IsConnected)
            dim frm = New frmSqlConfig
            frm.Title = "PS"
            frm.ShowDialog(me)
        End If
        My.Settings.Reload()
        Me.Location = My.Settings.screenposition


        'TODO: This line of code loads data into the 'DsPackingSolution.Facilities' table. You can move, or remove it, as needed.
        Me.FacilitiesTableAdapter.Fill(Me.DsPackingSolution.Facilities)
        _sFac = My.Settings.sFac
        _mGroup = My.Settings.mGroup

        FacilitiesBindingSource.Position = FacilitiesBindingSource.Find("fcfacility", _sFac)

        If Val(cbxShift.Text) <> 4 Then
            If (Now.Hour >= 8 And Now.Hour < 16) Then
                nShift = 1
                cbxShift.Text = 1
            End If
            If Now.Hour >= 16 And Now.Hour < 24 Then
                nShift = 2
                cbxShift.Text = 2
            End If
            If Now.Hour >= 24 Or Now.Hour < 8 Then
                nShift = 3
                cbxShift.Text = 3
            End If
        Else
            nShift = cbxShift.Text
        End If

        If nShift = 1 Then
            nShiftHours = 8 ' 8am for first shift
        ElseIf nShift = 2 Then
            nShiftHours = 20 ' 10pm for second shift
        Else
            nShiftHours = 23
        End If


        dvJoMast.Table = DsPackingSolution.jomast
        dvHTCrew.Table = DsPackingSolution.ht_crew
        dvProdSearch.Table = DsPackingSolution.prod_search
        dvSchedSearch.Table = DsPackingSolution.shift_list
        dvHTProd.Table = DsPackingSolution.ht_prod

        fillData()
        txtQty.Text = 0
        'txtJoSearch.Text = "Orden de Trabajo"
        txtQty.Enabled = False
        cmdAdd.Enabled = False
        txtJoSearch.Enabled = True
        bChgTamper = False
        dvHTCrew.RowFilter = "active = 'true'"
        dvHTCrew.Sort = "tamper"
        mnuShowDate.Checked = True
        cmdCheck_Click(Nothing, Nothing)

        cbxDate.MaxDate = Now.Date
        Try
            Using ta As New dsPackingSolutionTableAdapters.QueriesTableAdapter
                _SMTPClient = ta.spGetSMTPServerName(1).ToString.Trim
            End Using
        Catch ex As SqlException
            Using taError As New dsPackingSolutionTableAdapters.QueriesTableAdapter
                taError.insAppErrors(_sFac, Me.GetType.Name, ex.Message.ToString, System.Environment.UserName)
            End Using
        End Try


        TitleReset()
    End Sub

        Private Function GetSource(sConnArr As String(), dSource As String) As String

        For Each el As String In sConnArr
            Dim aEl As String() = el.Split("=")
            If el.Contains("Catalog") Then
                dSource = aEl(1)
                exit for
            End If
        Next
        Return dSource
    End Function

    private Sub TitleReset
        dim appSetting = new AppSettings()
        appSetting.Refersh()

        Dim connM2M as string() = appSetting.GetConnectionString("M2M").Split(";")
        Dim connPS as string() = appSetting.GetConnectionString("PS").Split(";")
        _m2mCatalog = GetSource(connM2M, _m2mCatalog)
        _psCatalog = GetSource(connPS, _psCatalog)
        Text = "08 HT Production Recorder v." + Assembly.GetExecutingAssembly().GetName().Version.ToString() + "     M2M: " + _m2mCatalog + " PS: " + _psCatalog

    End Sub

    Private Sub fillSched()
        Try
            Shift_listTableAdapter.Fill(Me.DsPackingSolution.shift_list, _sFac, nShift, Me.cbxDate.Value.Date)
        Catch ex As System.Exception
            Using apperrorAdapter As New dsPackingSolutionTableAdapters.app_errorsTableAdapter
                Try
                    apperrorAdapter.Insert(cbxFac.SelectedValue.ToString.Trim, "HT Production Recorder", ex.Message.ToString(), System.Environment.UserName, Now)
                Catch ex2 As Exception
                    MsgBox(ex2.Message.ToString())
                End Try
            End Using
            MsgBox(ex.Message.ToString())
        End Try
    End Sub
    Private Sub fillData()
        ''DsPackingSolution.Clear()
        Dim dv As New DataView
        Try
            Me.Early_dateTA.Fill(Me.DsPackingSolution.early_date)
            dv.Table = DsPackingSolution.early_date
            JomastTableAdapter.Fill(Me.DsPackingSolution.jomast, _sFac)
            Me.DaHTProd.FillBy(Me.DsPackingSolution.ht_prod, dv.Item(0).Item("fact_rel"), _sFac)
            Me.DaHTCrew.Fill(Me.DsPackingSolution.ht_crew)
            fillSched()
        Catch ex As Exception
            Using apperrorAdapter As New dsPackingSolutionTableAdapters.app_errorsTableAdapter
                Try
                    apperrorAdapter.Insert(cbxFac.SelectedValue.ToString.Trim, "HT Production Recorder", ex.Message.ToString(), System.Environment.UserName, Now)
                Catch ex2 As Exception
                    MsgBox(ex2.Message.ToString())
                End Try
            End Using
            MsgBox(ex.Message.ToString())
        End Try
        dv.Dispose()
    End Sub

    Private Sub txtQty_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtQty.TextChanged
        If Not IsNumeric(txtQty.Text) Then
            Beep()
            txtQty.Text = ""
        End If
    End Sub
    Private Sub txtJoSearch_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtJoSearch.TextChanged
        If txtJoSearch.Text.Length < 5 Then
            ' don't have a full job number yet
            Return
        End If
        Dim sFullJOName As String
        Dim arrValues(3) As Object

        Dim t As DataTable
        Dim myRow As DataRow
        Dim nSchedRow As Integer = 0
        Dim nCrewRow As Integer = 0
        Dim nTot As Integer
        Dim tamp_id As Integer
        Dim nCuft As Double
        Dim nRow As Integer

        ' set the shift if not using combo
        If mnuShowDate.Checked <> True Then
            If Val(cbxShift.Text) <> 4 Then
                If (Now.Hour >= 8 And Now.Hour < 16) Then
                    nShift = 1
                    cbxShift.Text = 1
                End If
                If Now.Hour >= 16 And Now.Hour < 24 Then
                    nShift = 2
                    cbxShift.Text = 2
                End If
                If Now.Hour >= 24 Or Now.Hour < 8 Then
                    nShift = 3
                    cbxShift.Text = 3
                End If
            Else
                nShift = cbxShift.Text
            End If
        Else
            nShift = cbxShift.Text
        End If

        If nShift = 1 Then
            nShiftHours = 8 ' 8am for first shift
        ElseIf nShift = 2 Then
            nShiftHours = 20 ' 10pm for second shift
        Else
            nShiftHours = 23
        End If

        ' make sure the shift is set...Else force shift to be set.
        If cbxShift.Text <> "" Then
            If txtJoSearch.TextLength = 5 Then
                sFullJOName = txtJoSearch.Text.ToUpper & "-0000"
                'MsgBox("inside", MsgBoxStyle.Information)
            Else
                sFullJOName = Trim(txtJoSearch.Text)
            End If
            ' Make sure we are seeing everything.
            dvJoMast.RowFilter = Nothing
            dvJoMast.Sort = "fjobno"
            Dim idx As Integer = JomastBindingSource.Find("fjobno", sFullJOName)

            If idx = -1 Then 'Job not found
                MsgBox(txtJoSearch.Text & " not found. Jo has to be in RELEASED status.", MsgBoxStyle.Information, "Looking for JO's")
                txtJoSearch.Text = ""
                txtJoSearch.Focus()
            Else
                ' job found in jo mast.
                JomastBindingSource.Position = idx
                Dim currJoMast As dsPackingSolution.jomastRow = DirectCast(JomastBindingSource.Current.Row, dsPackingSolution.jomastRow)
                nMakeQty = currJoMast.fquantity
                nCuft = currJoMast.fnusrqty1
                lblPart.Text = currJoMast.fpartno.Trim
                lblJob.Text = currJoMast.fjob_name.Trim
                If currJoMast.fjob_name.Length = 0 Then
                    lblJob.Text = "Stock"
                End If
                Dim sSoKey As String = currJoMast.fdescmemo

                If mnuShowDate.Checked <> True Then  ' no future production allowed.
                    If Now.Hour < 7 Then ' Is it still yesterday's production?
                        dTampdate = Now.Date.AddDays(-1).AddHours(23) ' day before at 11 pm
                    Else
                        dTampdate = Now.Date
                        ' MsgBox(dTampDate, MsgBoxStyle.Exclamation, "time")
                    End If
                Else
                    ' assume they are using the schedule date.
                    dTampdate = cbxDate.Value
                End If
                cbxShift_SelectedIndexChanged(Nothing, Nothing)
                If mnuShowDate.Checked = True Then
                    nShift = cbxShift.Text
                End If
                ' see if the jo has been scheduled for date and shift inquired about
                Dim nSchedSearchRow As Integer

                dvSchedSearch.Sort = "fjobno"
                nSchedSearchRow = dvSchedSearch.Find(sFullJOName.ToUpper.Trim)

                'sched_search(sFullJOName, dTampdate.Date, nShift)
                'If dvSchedSearch.Count > 0 Then ' the jo has been scheduled.
                If nSchedSearchRow <> -1 Then ' the jo has been scheduled.
                    dvSchedSearch.RowFilter = "fjobno = '" & sFullJOName & "'"

                    If dvSchedSearch.Count > 1 Then
                        Dim dr As DataRowView
                        Dim sFilter As String = ""
                        For Each dr In dvSchedSearch
                            'build the string 12,14,23
                            sFilter &= dr("tamper_id") & ","
                        Next
                        sFilter = Mid(sFilter, 1, Len(sFilter) - 1)

                        dvHTCrew.RowFilter = "id in (" & sFilter & ")"
                        'cbxTamper.Visible = True

                        Dim obj As New frmSelectTamper
                        obj.nShift = nShift
                        obj.sFilter = sFilter

                        obj.ShowDialog(Me)

                        tamp_id = obj.SelectedTamper
                        obj.Dispose()
                    Else
                        tamp_id = dvSchedSearch.Item(0).Item("tamper_id")
                    End If
                    dvHTCrew.RowFilter = Nothing
                    dvHTCrew.Sort = "ID"
                    nCrewRow = dvHTCrew.Find(tamp_id)
                    If nCrewRow <> -1 Then  ' crew member found.
                        lblTamper.Text = dvHTCrew.Item(nCrewRow).Item("tamper")
                    Else
                        lblTamper.Text = "not found"
                    End If

                    prod_search(sFullJOName, tamp_id, dTampdate, nShift)
                    If dvProdSearch.Count > 0 Then  ' modify existing ht_prod record

                        dvHTProd.Sort = "ID"
                        nRow = dvProdSearch.Item(0).Item("ID")

                        nRowCount = dvHTProd.Find(nRow)
                        If nRowCount = -1 Then MsgBox("oops. big problem", MsgBoxStyle.Information, " can't find...")


                        'MsgBox("Prod Search Count " & dvProdSearch.Count & " time to add production")
                        cmdAdd.Enabled = True
                        txtQty.Enabled = True
                        nTot = get_sum_prod(sFullJOName)
                        If nTot > 0 Then
                            txtQty.Text = nMakeQty - nTot
                        Else
                            txtQty.Text = nMakeQty
                        End If
                        gbxJoStats.Text = "Ready for production entry..."
                        If tamp_id = 0 Then
                            cmdAdd.Enabled = False
                            MsgBox("Please select tamper...", MsgBoxStyle.Information, "JO was scheduled before a tamper was selected for the station")
                        Else
                            txtQty.Focus()
                        End If
                    Else   ' insert new ht_prod record.
                        'MsgBox("Time to start job")
                        t = DsPackingSolution.Tables("ht_prod")
                        myRow = t.NewRow
                        cmdAdd.Enabled = False

                        txtQty.Enabled = False
                        'MsgBox("Get-R-Done")
                        myRow("shift") = nShift
                        myRow("tamper_id") = tamp_id
                        myRow("fjobno") = sFullJOName.ToUpper()
                        myRow("cuft") = nCuft
                        myRow("qty_prod") = 0
                        myRow("start") = Now
                        myRow("finish") = Now.AddDays(-1)
                        myRow("in_process") = True
                        myRow("sys_date") = Now
                        myRow("mgroup") = currJoMast.mgroup
                        myRow("fsono") = Mid(sSoKey, 1, 6)
                        myRow("finumber") = Mid(sSoKey, 8, 3)
                        myRow("frelease") = Mid(sSoKey, 12, 3)
                        myRow("pfac") = _sFac
                        myRow("fac") = dvSchedSearch.Item(0).Item("fac")
                        t.Rows.Add(myRow)
                        lblTotal.Text = "Trabajo Comenzado"
                        nRowCount = -1
                        cmdAdd_Click(Nothing, Nothing)
                        myRow = Nothing
                    End If
                    dvSchedSearch.RowFilter = Nothing

                Else
                    MsgBox("Schedule this job before entering production.", MsgBoxStyle.Information, "Please...")
                    txtJoSearch.Text = ""
                    txtJoSearch.Focus()
                End If
                gbxJoStats.Text = "Job: " & txtJoSearch.Text
            End If
        Else
            cbxShift.Focus()
            txtJoSearch.Text = ""
        End If
    End Sub
    Private Function get_sum_prod(ByVal sJO As String)
        Dim dr As DataRowView
        Dim drv As DataView
        Dim nTot As Integer
        nTot = 0

        dvHTProd.RowFilter = "fjobno = '" & sJO & "'"
        drv = dvHTProd
        For Each dr In drv
            nTot += dr("qty_prod")
        Next
        lblTotal.Text = "Total produced for " & sJO & ":  " & nTot & " and " & nMakeQty - nTot & " remaining."
        dvHTProd.RowFilter = Nothing
        Return nTot
    End Function

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If nRowCount <> -1 Then
            dvHTProd.Item(nRowCount).Item("qty_prod") = Val(txtQty.Text)
            If mnuShowDate.Checked Then
                dvHTProd.Item(nRowCount).Item("finish") = cbxDate.Value.AddHours(nShiftHours)
                'dvHTprod.Item(nRowCount).Item("start") = cbxDate.Value.AddHours(nShiftHours)
            Else
                If nShift = 3 Then
                    If Now.Hour < 7 Then
                        ' we are in the early morning  need to back up a day
                        cbxDate.Value = Now.Date.AddDays(-1)
                    End If
                    dvHTProd.Item(nRowCount).Item("finish") = cbxDate.Value.AddHours(nShiftHours)
                Else
                    dvHTProd.Item(nRowCount).Item("finish") = Now
                End If
                'dvHTprod.Item(nRowCount).Item("start")
            End If
            dvHTProd.Item(nRowCount).Item("in_process") = False
            dvHTProd.Item(nRowCount).Item("sys_date") = Now
        End If
        If Val(txtQty.Text) = 0 Then ' ditch the zero quantities becuase they are screwing up my reporting.
            'dvHTProd.Item(nRowCount).Delete()
        End If
        Try
            DaHTProd.Update(DsPackingSolution)
            DsPackingSolution.AcceptChanges()
            If nRowCount <> -1 Then
                get_sum_prod(dvHTProd.Item(nRowCount).Item("fjobno"))
            End If
        Catch ex As Exception
            Using apperrorAdapter As New dsPackingSolutionTableAdapters.app_errorsTableAdapter
                Try
                    apperrorAdapter.Insert(cbxFac.SelectedValue.ToString.Trim, "HT Production Recorder", ex.Message.ToString(), System.Environment.UserName, Now)
                Catch ex2 As Exception
                    MsgBox(ex2.Message.ToString())
                End Try
            End Using
        End Try
        dvHTProd.RowFilter = Nothing
        'dvHTCrew.RowFilter = Nothing

        fillData()
        txtJoSearch.Text = ""
        txtQty.Text = 0
        txtQty.Enabled = False
        cmdAdd.Enabled = False
        txtJoSearch.Focus()
    End Sub

    Private Sub mnuabout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
        ShowAboutBox()
    End Sub
    Public Sub ShowAboutBox()
        Dim objAboutBox As New About
        objAboutBox.ShowDialog(Me)
    End Sub

    Private Sub mnuExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExit.Click
        End
    End Sub
    Private Sub cbxShift_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxShift.SelectedIndexChanged
        'dvHT_sched.RowFilter = "shift = " & cbxShift.Text & " and tamp_date = '" & dTampdate & "'"
        txtJoSearch.Enabled = True
        txtJoSearch.Focus()
        fillSched()
    End Sub

    Private Sub mnuRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRefresh.Click
        fillData()
    End Sub

    Private Sub cbxShift_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxShift.TextChanged
        If IsNumeric(cbxShift.Text) Then
            If Val(cbxShift.Text) < 1 Or Val(cbxShift.Text) > 4 Then
                Beep()
                cbxShift.Text = 1
                MsgBox("try selecting a valid number from the drop down box", MsgBoxStyle.Information, "a freindly helpful hint")
            Else
                cbxShift_SelectedIndexChanged(Nothing, Nothing)
            End If
        Else
            Beep()
            cbxShift.Text = 1
            cbxShift_SelectedIndexChanged(Nothing, Nothing)
        End If
    End Sub

    Private Sub cbxTamper_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cmdAdd.Enabled = True
        bChgTamper = True
        txtQty.Focus()
    End Sub

    Private Sub MnuShowDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowDate.Click
        cbxDate.Visible = Not cbxDate.Visible
        cbxShift.Visible = Not cbxShift.Visible
        lblShift.Visible = Not lblShift.Visible
        Me.mnuShowDate.Checked = Not Me.mnuShowDate.Checked
        'cmdCheck_Click(Nothing, Nothing)
    End Sub
    Private Sub sched_row(ByVal jo As String, ByVal dDate As Date, ByVal shift As Integer, ByVal tamp_id As Integer)
        'daHT_sched.SelectCommand.Parameters("fjobno").Value = jo
        'daHT_sched.SelectCommand.Parameters("tamp_date").Value = dDate
        'daHT_sched.SelectCommand.Parameters("shift").Value = shift
        'daHT_sched.SelectCommand.Parameters("tamper_id").Value = tamp_id

        'DaHTSched.Fill(DsPackingSolution.ht_sched, dDate, shift, jo, tamp_id, sCell)

        'Try
        '    daHT_sched.Fill(DsPackingSolution)
        'Catch ex As Exception
        '    MsgBox(ex.Message.ToString, MsgBoxStyle.Information, "fill ht_sched")
        'End Try
    End Sub
    Private Sub sched_search(ByVal jo As String, ByVal dDate As Date, ByVal shift As Integer)
        'DaSchedSearch.SelectCommand.Parameters("fjobno").Value = jo
        'DaSchedSearch.SelectCommand.Parameters("tamp_date").Value = dDate
        'DaSchedSearch.SelectCommand.Parameters("shift").Value = shift
        DaSchedSearch.Fill(DsPackingSolution.sched_search, dDate, shift, jo)
        'Try
        '    DaSchedSearch.Fill(DsPackingSolution)
        'Catch ex As Exception
        '    MsgBox(ex.Message.ToString, MsgBoxStyle.Information, "fill sched search")
        'End Try
    End Sub
    Private Sub prod_search(ByVal jobno As String, ByVal tampid As Integer, ByVal dDate As Date, ByVal shift As Integer)
        'DaProdSearch.SelectCommand.Parameters("fjobno").Value = jobno
        'DaProdSearch.SelectCommand.Parameters("tamper_id").Value = tampid
        'DaProdSearch.SelectCommand.Parameters("sys_date").Value = dDate
        'DaProdSearch.SelectCommand.Parameters("shift").Value = shift


        DaProdSearch.Fill(DsPackingSolution.prod_search, jobno, tampid, shift, dDate)
        'Try
        '    DaProdSearch.Fill(DsPackingSolution)
        'Catch ex As Exception
        '    MsgBox(ex.Message.ToString, MsgBoxStyle.Information, "fill prod search")
        'End Try
    End Sub

    Private Sub cbxDate_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxDate.GotFocus
        cbxDate.MaxDate = Now.Date
    End Sub

    Private Sub Form1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.GotFocus
        cbxDate.MaxDate = Now

    End Sub

    Private Sub cmdCheck_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCheck.Click
        ' Me.mnuShowDate.Checked = Not Me.mnuShowDate.Checked
        MnuShowDate_Click(Nothing, Nothing)
    End Sub

    Private Sub cbxDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxDate.TextChanged
        fillData()
    End Sub

    Private Sub M2MToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
        Handles M2MToolStripMenuItem.Click
        dim frm = New frmSqlConfig
        frm.Title = "M2M"
        frm.ShowDialog(me)
        If frm.Saved Then
            TitleReset()
        End If
    End Sub

    Private Sub PackingSolutionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
        Handles PackingSolutionToolStripMenuItem.Click
        dim frm = New frmSqlConfig
        frm.Title = "PS"
        frm.ShowDialog(me)
        If frm.Saved Then
            TitleReset()
        End If
    End Sub

    Private Sub cbxFac_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxFac.SelectionChangeCommitted
        Dim cbx As ComboBox = CType(sender, ComboBox)

        If cbx.SelectedValue.ToString.Trim <> _sFac Then
            _sFac = cbx.SelectedValue.ToString.Trim
            Cursor.Current = Cursors.WaitCursor
            fillData()
            txtJoSearch.Focus()
            Cursor.Current = Cursors.Default
        End If
    End Sub

    Private Sub ShowM2MConnToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ShowM2MConnToolStripMenuItem.Click
        dim appSettings = New AppSettings()
        dim connStr = appSettings.GetConnectionString("M2M")
        Messagebox.Show(connStr)
    End Sub

    Private Sub ShowPSConnToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ShowPSConnToolStripMenuItem.Click
        dim appSettings = New AppSettings()
        dim connStr = appSettings.GetConnectionString("PS")
        Messagebox.Show(connStr)
    End Sub

End Class
