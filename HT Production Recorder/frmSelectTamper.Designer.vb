﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSelectTamper
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim TamperLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSelectTamper))
        Me.DsPackingSolution = New HT_Production_Recorder.dsPackingSolution
        Me.Select_tamperBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DaTamper = New HT_Production_Recorder.dsPackingSolutionTableAdapters.daTamper
        Me.TableAdapterManager = New HT_Production_Recorder.dsPackingSolutionTableAdapters.TableAdapterManager
        Me.TamperComboBox = New System.Windows.Forms.ComboBox
        TamperLabel = New System.Windows.Forms.Label
        CType(Me.DsPackingSolution, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Select_tamperBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TamperLabel
        '
        TamperLabel.AutoSize = True
        TamperLabel.Location = New System.Drawing.Point(21, 15)
        TamperLabel.Name = "TamperLabel"
        TamperLabel.Size = New System.Drawing.Size(42, 13)
        TamperLabel.TabIndex = 2
        TamperLabel.Text = "tamper:"
        '
        'DsPackingSolution
        '
        Me.DsPackingSolution.DataSetName = "dsPackingSolution"
        Me.DsPackingSolution.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Select_tamperBindingSource
        '
        Me.Select_tamperBindingSource.DataMember = "select_tamper"
        Me.Select_tamperBindingSource.DataSource = Me.DsPackingSolution
        '
        'DaTamper
        '
        Me.DaTamper.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.daHTCrew = Nothing
        Me.TableAdapterManager.daHTProd = Nothing
        'Me.TableAdapterManager.daHTSched = Nothing
        Me.TableAdapterManager.daProdSearch = Nothing
        Me.TableAdapterManager.daTamper = Me.DaTamper
        Me.TableAdapterManager.UpdateOrder = HT_Production_Recorder.dsPackingSolutionTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'TamperComboBox
        '
        Me.TamperComboBox.FormattingEnabled = True
        Me.TamperComboBox.Location = New System.Drawing.Point(69, 12)
        Me.TamperComboBox.Name = "TamperComboBox"
        Me.TamperComboBox.Size = New System.Drawing.Size(203, 21)
        Me.TamperComboBox.TabIndex = 3
        '
        'frmSelectTamper
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(289, 55)
        Me.Controls.Add(TamperLabel)
        Me.Controls.Add(Me.TamperComboBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmSelectTamper"
        Me.Text = "Please select tamper"
        CType(Me.DsPackingSolution, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Select_tamperBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DsPackingSolution As HT_Production_Recorder.dsPackingSolution
    Friend WithEvents Select_tamperBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DaTamper As HT_Production_Recorder.dsPackingSolutionTableAdapters.daTamper
    Friend WithEvents TableAdapterManager As HT_Production_Recorder.dsPackingSolutionTableAdapters.TableAdapterManager
    Friend WithEvents TamperComboBox As System.Windows.Forms.ComboBox
End Class
