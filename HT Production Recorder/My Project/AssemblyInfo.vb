﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("08 HT Production Recorder x86")>
<Assembly: AssemblyDescription("a product of Bridge Software Technologies")> 
<Assembly: AssemblyCompany("Reading Rock Inc")> 
<Assembly: AssemblyProduct("08 HT Production Recorder")> 
<Assembly: AssemblyCopyright("Copyright ©  2008")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("b59841c5-e242-4ca5-9357-db12906f1f85")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2.0.1.0")>
<Assembly: AssemblyFileVersion("1.0.0.0")> 
