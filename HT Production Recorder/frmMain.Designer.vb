﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.gbxJoStats = New System.Windows.Forms.GroupBox()
        Me.lblJob = New System.Windows.Forms.Label()
        Me.lblPart = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.TextBox()
        Me.HtschedBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsPackingSolution = New HT_Production_Recorder.dsPackingSolution()
        Me.cbxDate = New System.Windows.Forms.DateTimePicker()
        Me.lblShift = New System.Windows.Forms.Label()
        Me.cbxShift = New System.Windows.Forms.ComboBox()
        Me.lblTamper = New System.Windows.Forms.TextBox()
        Me.txtQty = New System.Windows.Forms.TextBox()
        Me.lblJO = New System.Windows.Forms.Label()
        Me.txtJoSearch = New System.Windows.Forms.TextBox()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuRefresh = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuShowDate = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdminToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReconfigDBToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.M2MToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowM2MConnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PackingSolutionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowPSConnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHelp = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmdCheck = New System.Windows.Forms.Button()
        Me.Ht_crewBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DaHTCrew = New HT_Production_Recorder.dsPackingSolutionTableAdapters.daHTCrew()
        Me.TableAdapterManager = New HT_Production_Recorder.dsPackingSolutionTableAdapters.TableAdapterManager()
        Me.App_errorsTableAdapter = New HT_Production_Recorder.dsPackingSolutionTableAdapters.app_errorsTableAdapter()
        Me.DaProdSearch = New HT_Production_Recorder.dsPackingSolutionTableAdapters.daProdSearch()
        Me.Prod_searchBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sched_searchBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DaSchedSearch = New HT_Production_Recorder.dsPackingSolutionTableAdapters.daSchedSearch()
        Me.Early_dateBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Early_dateTA = New HT_Production_Recorder.dsPackingSolutionTableAdapters.early_dateTA()
        Me.App_errorsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.cbxFac = New System.Windows.Forms.ComboBox()
        Me.FacilitiesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DaHTSched = New HT_Production_Recorder.dsPackingSolutionTableAdapters.daHTSched()
        Me.DaHTProdBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ht_prodBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DaHTProd = New HT_Production_Recorder.dsPackingSolutionTableAdapters.daHTProd()
        Me.FacilitiesTableAdapter = New HT_Production_Recorder.dsPackingSolutionTableAdapters.FacilitiesTableAdapter()
        Me.Shift_listBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Shift_listTableAdapter = New HT_Production_Recorder.dsPackingSolutionTableAdapters.shift_listTableAdapter()
        Me.JomastBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.JomastTableAdapter = New HT_Production_Recorder.dsPackingSolutionTableAdapters.jomastTableAdapter()
        Me.lblFac = New System.Windows.Forms.Label()
        Me.gbxJoStats.SuspendLayout
        CType(Me.HtschedBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DsPackingSolution,System.ComponentModel.ISupportInitialize).BeginInit
        Me.MenuStrip1.SuspendLayout
        CType(Me.Ht_crewBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Prod_searchBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Sched_searchBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Early_dateBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.App_errorsBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.FacilitiesBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DaHTProdBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Ht_prodBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Shift_listBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.JomastBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'gbxJoStats
        '
        Me.gbxJoStats.Controls.Add(Me.lblJob)
        Me.gbxJoStats.Controls.Add(Me.lblPart)
        Me.gbxJoStats.Controls.Add(Me.lblTotal)
        Me.gbxJoStats.Location = New System.Drawing.Point(28, 88)
        Me.gbxJoStats.Name = "gbxJoStats"
        Me.gbxJoStats.Size = New System.Drawing.Size(767, 232)
        Me.gbxJoStats.TabIndex = 0
        Me.gbxJoStats.TabStop = false
        '
        'lblJob
        '
        Me.lblJob.AutoSize = true
        Me.lblJob.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblJob.Location = New System.Drawing.Point(34, 85)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(50, 25)
        Me.lblJob.TabIndex = 4
        Me.lblJob.Text = "Job"
        '
        'lblPart
        '
        Me.lblPart.AutoSize = true
        Me.lblPart.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblPart.Location = New System.Drawing.Point(34, 39)
        Me.lblPart.Name = "lblPart"
        Me.lblPart.Size = New System.Drawing.Size(55, 25)
        Me.lblPart.TabIndex = 3
        Me.lblPart.Text = "Part"
        '
        'lblTotal
        '
        Me.lblTotal.Location = New System.Drawing.Point(34, 141)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.ReadOnly = true
        Me.lblTotal.Size = New System.Drawing.Size(690, 20)
        Me.lblTotal.TabIndex = 2
        '
        'HtschedBindingSource
        '
        Me.HtschedBindingSource.DataMember = "ht_sched"
        Me.HtschedBindingSource.DataSource = Me.DsPackingSolution
        '
        'DsPackingSolution
        '
        Me.DsPackingSolution.DataSetName = "dsPackingSolution"
        Me.DsPackingSolution.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'cbxDate
        '
        Me.cbxDate.Location = New System.Drawing.Point(28, 62)
        Me.cbxDate.Name = "cbxDate"
        Me.cbxDate.Size = New System.Drawing.Size(200, 20)
        Me.cbxDate.TabIndex = 1
        '
        'lblShift
        '
        Me.lblShift.AutoSize = true
        Me.lblShift.Location = New System.Drawing.Point(145, 33)
        Me.lblShift.Name = "lblShift"
        Me.lblShift.Size = New System.Drawing.Size(31, 13)
        Me.lblShift.TabIndex = 2
        Me.lblShift.Text = "turno"
        '
        'cbxShift
        '
        Me.cbxShift.FormattingEnabled = true
        Me.cbxShift.Items.AddRange(New Object() {"1", "2", "3", "4"})
        Me.cbxShift.Location = New System.Drawing.Point(182, 29)
        Me.cbxShift.Name = "cbxShift"
        Me.cbxShift.Size = New System.Drawing.Size(46, 21)
        Me.cbxShift.TabIndex = 3
        '
        'lblTamper
        '
        Me.lblTamper.Location = New System.Drawing.Point(234, 30)
        Me.lblTamper.Name = "lblTamper"
        Me.lblTamper.ReadOnly = true
        Me.lblTamper.Size = New System.Drawing.Size(298, 20)
        Me.lblTamper.TabIndex = 4
        Me.lblTamper.Text = "Tamper"
        '
        'txtQty
        '
        Me.txtQty.Location = New System.Drawing.Point(551, 30)
        Me.txtQty.Name = "txtQty"
        Me.txtQty.Size = New System.Drawing.Size(38, 20)
        Me.txtQty.TabIndex = 5
        '
        'lblJO
        '
        Me.lblJO.AutoSize = true
        Me.lblJO.Location = New System.Drawing.Point(604, 33)
        Me.lblJO.Name = "lblJO"
        Me.lblJO.Size = New System.Drawing.Size(20, 13)
        Me.lblJO.TabIndex = 6
        Me.lblJO.Text = "JO"
        '
        'txtJoSearch
        '
        Me.txtJoSearch.Location = New System.Drawing.Point(630, 30)
        Me.txtJoSearch.Name = "txtJoSearch"
        Me.txtJoSearch.Size = New System.Drawing.Size(68, 20)
        Me.txtJoSearch.TabIndex = 7
        '
        'cmdAdd
        '
        Me.cmdAdd.Location = New System.Drawing.Point(720, 28)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(75, 23)
        Me.cmdAdd.TabIndex = 8
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.UseVisualStyleBackColor = true
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFile, Me.AdminToolStripMenuItem, Me.mnuHelp})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(807, 24)
        Me.MenuStrip1.TabIndex = 9
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuFile
        '
        Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuRefresh, Me.mnuShowDate, Me.mnuExit})
        Me.mnuFile.Name = "mnuFile"
        Me.mnuFile.Size = New System.Drawing.Size(37, 20)
        Me.mnuFile.Text = "&File"
        '
        'mnuRefresh
        '
        Me.mnuRefresh.Name = "mnuRefresh"
        Me.mnuRefresh.Size = New System.Drawing.Size(130, 22)
        Me.mnuRefresh.Text = "Refresh"
        '
        'mnuShowDate
        '
        Me.mnuShowDate.CheckOnClick = true
        Me.mnuShowDate.Name = "mnuShowDate"
        Me.mnuShowDate.Size = New System.Drawing.Size(130, 22)
        Me.mnuShowDate.Text = "Show Date"
        '
        'mnuExit
        '
        Me.mnuExit.Name = "mnuExit"
        Me.mnuExit.Size = New System.Drawing.Size(130, 22)
        Me.mnuExit.Text = "E&xit"
        '
        'AdminToolStripMenuItem
        '
        Me.AdminToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReconfigDBToolStripMenuItem})
        Me.AdminToolStripMenuItem.Name = "AdminToolStripMenuItem"
        Me.AdminToolStripMenuItem.Size = New System.Drawing.Size(55, 20)
        Me.AdminToolStripMenuItem.Text = "Admin"
        '
        'ReconfigDBToolStripMenuItem
        '
        Me.ReconfigDBToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.M2MToolStripMenuItem, Me.PackingSolutionToolStripMenuItem})
        Me.ReconfigDBToolStripMenuItem.Name = "ReconfigDBToolStripMenuItem"
        Me.ReconfigDBToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.ReconfigDBToolStripMenuItem.Text = "Reconfig DB"
        '
        'M2MToolStripMenuItem
        '
        Me.M2MToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ShowM2MConnToolStripMenuItem})
        Me.M2MToolStripMenuItem.Name = "M2MToolStripMenuItem"
        Me.M2MToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.M2MToolStripMenuItem.Text = "M2M"
        '
        'ShowM2MConnToolStripMenuItem
        '
        Me.ShowM2MConnToolStripMenuItem.Name = "ShowM2MConnToolStripMenuItem"
        Me.ShowM2MConnToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.ShowM2MConnToolStripMenuItem.Text = "Show M2M Conn"
        '
        'PackingSolutionToolStripMenuItem
        '
        Me.PackingSolutionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ShowPSConnToolStripMenuItem})
        Me.PackingSolutionToolStripMenuItem.Name = "PackingSolutionToolStripMenuItem"
        Me.PackingSolutionToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.PackingSolutionToolStripMenuItem.Text = "Packing Solution"
        '
        'ShowPSConnToolStripMenuItem
        '
        Me.ShowPSConnToolStripMenuItem.Name = "ShowPSConnToolStripMenuItem"
        Me.ShowPSConnToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ShowPSConnToolStripMenuItem.Text = "Show PS Conn"
        '
        'mnuHelp
        '
        Me.mnuHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAbout})
        Me.mnuHelp.Name = "mnuHelp"
        Me.mnuHelp.Size = New System.Drawing.Size(44, 20)
        Me.mnuHelp.Text = "Help"
        '
        'mnuAbout
        '
        Me.mnuAbout.Name = "mnuAbout"
        Me.mnuAbout.Size = New System.Drawing.Size(157, 22)
        Me.mnuAbout.Text = " &About HT Prod"
        '
        'cmdCheck
        '
        Me.cmdCheck.Location = New System.Drawing.Point(720, 57)
        Me.cmdCheck.Name = "cmdCheck"
        Me.cmdCheck.Size = New System.Drawing.Size(75, 23)
        Me.cmdCheck.TabIndex = 8
        Me.cmdCheck.Text = "Check"
        Me.cmdCheck.UseVisualStyleBackColor = true
        '
        'Ht_crewBindingSource
        '
        Me.Ht_crewBindingSource.DataMember = "ht_crew"
        Me.Ht_crewBindingSource.DataSource = Me.DsPackingSolution
        '
        'DaHTCrew
        '
        Me.DaHTCrew.ClearBeforeFill = true
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.app_errorsTableAdapter = Me.App_errorsTableAdapter
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = false
        Me.TableAdapterManager.daHTCrew = Me.DaHTCrew
        Me.TableAdapterManager.daHTProd = Nothing
        Me.TableAdapterManager.daProdSearch = Nothing
        Me.TableAdapterManager.daTamper = Nothing
        Me.TableAdapterManager.UpdateOrder = HT_Production_Recorder.dsPackingSolutionTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'App_errorsTableAdapter
        '
        Me.App_errorsTableAdapter.ClearBeforeFill = true
        '
        'DaProdSearch
        '
        Me.DaProdSearch.ClearBeforeFill = true
        '
        'Prod_searchBindingSource
        '
        Me.Prod_searchBindingSource.DataMember = "prod_search"
        Me.Prod_searchBindingSource.DataSource = Me.DsPackingSolution
        '
        'Sched_searchBindingSource
        '
        Me.Sched_searchBindingSource.DataMember = "sched_search"
        Me.Sched_searchBindingSource.DataSource = Me.DsPackingSolution
        '
        'DaSchedSearch
        '
        Me.DaSchedSearch.ClearBeforeFill = true
        '
        'Early_dateBindingSource
        '
        Me.Early_dateBindingSource.DataMember = "early_date"
        Me.Early_dateBindingSource.DataSource = Me.DsPackingSolution
        '
        'Early_dateTA
        '
        Me.Early_dateTA.ClearBeforeFill = true
        '
        'App_errorsBindingSource
        '
        Me.App_errorsBindingSource.DataMember = "app_errors"
        Me.App_errorsBindingSource.DataSource = Me.DsPackingSolution
        '
        'cbxFac
        '
        Me.cbxFac.DataSource = Me.FacilitiesBindingSource
        Me.cbxFac.DisplayMember = "fcfacility"
        Me.cbxFac.FormattingEnabled = true
        Me.cbxFac.Location = New System.Drawing.Point(56, 30)
        Me.cbxFac.Name = "cbxFac"
        Me.cbxFac.Size = New System.Drawing.Size(75, 21)
        Me.cbxFac.TabIndex = 12
        Me.cbxFac.ValueMember = "fcfacility"
        '
        'FacilitiesBindingSource
        '
        Me.FacilitiesBindingSource.DataMember = "Facilities"
        Me.FacilitiesBindingSource.DataSource = Me.DsPackingSolution
        '
        'DaHTSched
        '
        Me.DaHTSched.ClearBeforeFill = true
        '
        'DaHTProdBindingSource
        '
        Me.DaHTProdBindingSource.DataSource = GetType(HT_Production_Recorder.dsPackingSolutionTableAdapters.daHTProd)
        '
        'Ht_prodBindingSource
        '
        Me.Ht_prodBindingSource.DataMember = "ht_prod"
        Me.Ht_prodBindingSource.DataSource = Me.DsPackingSolution
        '
        'DaHTProd
        '
        Me.DaHTProd.ClearBeforeFill = true
        '
        'FacilitiesTableAdapter
        '
        Me.FacilitiesTableAdapter.ClearBeforeFill = true
        '
        'Shift_listBindingSource
        '
        Me.Shift_listBindingSource.DataMember = "shift_list"
        Me.Shift_listBindingSource.DataSource = Me.DsPackingSolution
        '
        'Shift_listTableAdapter
        '
        Me.Shift_listTableAdapter.ClearBeforeFill = true
        '
        'JomastBindingSource
        '
        Me.JomastBindingSource.DataMember = "jomast"
        Me.JomastBindingSource.DataSource = Me.DsPackingSolution
        '
        'JomastTableAdapter
        '
        Me.JomastTableAdapter.ClearBeforeFill = true
        '
        'lblFac
        '
        Me.lblFac.AutoSize = true
        Me.lblFac.Location = New System.Drawing.Point(25, 33)
        Me.lblFac.Name = "lblFac"
        Me.lblFac.Size = New System.Drawing.Size(25, 13)
        Me.lblFac.TabIndex = 13
        Me.lblFac.Text = "Fac"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(807, 351)
        Me.Controls.Add(Me.lblFac)
        Me.Controls.Add(Me.cbxFac)
        Me.Controls.Add(Me.cmdCheck)
        Me.Controls.Add(Me.cmdAdd)
        Me.Controls.Add(Me.txtJoSearch)
        Me.Controls.Add(Me.lblJO)
        Me.Controls.Add(Me.txtQty)
        Me.Controls.Add(Me.lblTamper)
        Me.Controls.Add(Me.cbxShift)
        Me.Controls.Add(Me.lblShift)
        Me.Controls.Add(Me.cbxDate)
        Me.Controls.Add(Me.gbxJoStats)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMain"
        Me.Text = "HT Production Recorder (ALM) x86"
        Me.gbxJoStats.ResumeLayout(false)
        Me.gbxJoStats.PerformLayout
        CType(Me.HtschedBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DsPackingSolution,System.ComponentModel.ISupportInitialize).EndInit
        Me.MenuStrip1.ResumeLayout(false)
        Me.MenuStrip1.PerformLayout
        CType(Me.Ht_crewBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Prod_searchBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Sched_searchBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Early_dateBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.App_errorsBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.FacilitiesBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DaHTProdBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Ht_prodBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Shift_listBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.JomastBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents gbxJoStats As System.Windows.Forms.GroupBox
    Friend WithEvents cbxDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblShift As System.Windows.Forms.Label
    Friend WithEvents cbxShift As System.Windows.Forms.ComboBox
    Friend WithEvents lblTamper As System.Windows.Forms.TextBox
    Friend WithEvents txtQty As System.Windows.Forms.TextBox
    Friend WithEvents lblJO As System.Windows.Forms.Label
    Friend WithEvents txtJoSearch As System.Windows.Forms.TextBox
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents lblTotal As System.Windows.Forms.TextBox
    Friend WithEvents DsPackingSolution As HT_Production_Recorder.dsPackingSolution
    Friend WithEvents Ht_crewBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DaHTCrew As HT_Production_Recorder.dsPackingSolutionTableAdapters.daHTCrew
    Friend WithEvents TableAdapterManager As HT_Production_Recorder.dsPackingSolutionTableAdapters.TableAdapterManager
    Friend WithEvents DaProdSearch As HT_Production_Recorder.dsPackingSolutionTableAdapters.daProdSearch
    Friend WithEvents Prod_searchBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sched_searchBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DaSchedSearch As HT_Production_Recorder.dsPackingSolutionTableAdapters.daSchedSearch
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuFile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuHelp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRefresh As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuShowDate As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmdCheck As System.Windows.Forms.Button
    Friend WithEvents Early_dateBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Early_dateTA As HT_Production_Recorder.dsPackingSolutionTableAdapters.early_dateTA
    Friend WithEvents App_errorsTableAdapter As HT_Production_Recorder.dsPackingSolutionTableAdapters.app_errorsTableAdapter
    Friend WithEvents App_errorsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AdminToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReconfigDBToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents M2MToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PackingSolutionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cbxFac As System.Windows.Forms.ComboBox
    Friend WithEvents HtschedBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DaHTSched As HT_Production_Recorder.dsPackingSolutionTableAdapters.daHTSched
    Friend WithEvents DaHTProdBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Ht_prodBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DaHTProd As HT_Production_Recorder.dsPackingSolutionTableAdapters.daHTProd
    Friend WithEvents FacilitiesBindingSource As BindingSource
    Friend WithEvents FacilitiesTableAdapter As dsPackingSolutionTableAdapters.FacilitiesTableAdapter
    Friend WithEvents Shift_listBindingSource As BindingSource
    Friend WithEvents Shift_listTableAdapter As dsPackingSolutionTableAdapters.shift_listTableAdapter
    Friend WithEvents JomastBindingSource As BindingSource
    Friend WithEvents JomastTableAdapter As dsPackingSolutionTableAdapters.jomastTableAdapter
    Friend WithEvents lblFac As Label
    Friend WithEvents lblJob As Label
    Friend WithEvents lblPart As Label
    Friend WithEvents ShowM2MConnToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ShowPSConnToolStripMenuItem As ToolStripMenuItem
End Class
