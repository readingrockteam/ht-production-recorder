﻿Public Class frmSelectTamper
    Public nShift As Integer
    Public sFilter As String
    Private tamp_id As Integer
    Dim dvTamper As New DataView

    Public Property SelectedTamper() As Integer
        Get
            Return tamp_id
        End Get
        Set(ByVal value As Integer)

        End Set
    End Property


    Private Sub frmSelectTamper_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dvTamper.Table = DsPackingSolution.select_tamper
        fill_ds(nShift)
        TamperComboBox.DataSource = dvTamper
        TamperComboBox.DisplayMember = "tamper"
        TamperComboBox.ValueMember = "ID"

    End Sub
    Private Sub fill_ds(ByVal shift As Integer)
        DsPackingSolution.select_tamper.Clear()

        Try
            Me.DaTamper.Fill(Me.DsPackingSolution.select_tamper, shift)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        dvTamper.RowFilter = "id in (" & sFilter & ")"
    End Sub

    Private Sub TamperComboBox_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles TamperComboBox.SelectionChangeCommitted
        tamp_id = TamperComboBox.SelectedValue
        Close()
    End Sub
End Class